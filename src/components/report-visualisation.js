import React, { useEffect, useState } from 'react';
import propTypes from 'prop-types';
import { parseString } from 'xml2js';
import Report from './report';

// From https://github.com/lukejpreston/xunit-viewer/blob/master/parser/index.js
function normaliseResult(data) {
  let reports = [];

  if (!data) {
    return [];
  }

  if (data.testsuites && data.testsuites.testsuite) {
    if (Array.isArray(data.testsuites.testsuite)) {
      reports = data.testsuites.testsuite;
    } else {
      reports.push(data.testsuites.testsuite);
    }
  }

  if (data.testsuite) {
    if (Array.isArray(data.testsuite)) {
      reports = data.testsuite;
    } else {
      reports.push(data.testsuite);
    }
  }

  if (data.testcase) {
    if (Array.isArray(data.testcase)) {
      reports = [
        {
          testcase: data.testcase
        }
      ];
    } else {
      reports.push({
        testcase: [data.testcase]
      });
    }
  }

  return reports;
}

export default function ReportVisualisation({ xunitReportString }) {
  const [reports, setReports] = useState(null);

  useEffect(() => {
    parseString(xunitReportString, (err, result) => {
      if (!err) {
        setReports(normaliseResult(result));
      } else {
        // TODO
      }
    });
  }, [xunitReportString]);

  return (
    <div>
      {!!reports &&
        reports.map(({ testcase, $: reportMeta }) => (
          <Report
            tests={testcase}
            meta={reportMeta}
            key={`${reportMeta.name} _ ${reportMeta.timestamp}`}
          />
        ))}
    </div>
  );
}

ReportVisualisation.propTypes = {
  xunitReportString: propTypes.string
};
