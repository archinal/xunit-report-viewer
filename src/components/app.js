import React, {useState} from 'react';
import styled from 'styled-components';
import ReportVisualisation from "./report-visualisation";
import XunitInput from "./xunit-input";

const AppBody = styled.main`
  padding: 0 28px;
  margin: 0 auto;
  
  @media(min-width: 1280px) {
    width: 1280px;
  }
`;

const HorizontalLine = styled.hr`
    margin: 21px 0;
`;

function App() {
    const [xunitReportString, setXunitReportString] = useState('');

    return (
        <AppBody>
            <header>
                <h1>Xunit Report Viewer</h1>
            </header>
            <p>Paste or Upload your Xunit Report below to visualise the results</p>
            <XunitInput
                onChangeValue={setXunitReportString}
            />
            {!!xunitReportString && (
                <React.Fragment>
                    <HorizontalLine />
                    <ReportVisualisation xunitReportString={xunitReportString}/>
                </React.Fragment>
            )}
        </AppBody>
    );
}

export default App;
