import React from 'react';
import styled from 'styled-components';

const Header = styled.h3`
    margin: 14px 0 6px;
    font-size: 16px;
    font-weight: normal;
`;

const Metadata = styled.div`
    font-size: 12px;
    color: slategrey;
`;

const ErrorMessage = styled.p`
    margin: 6px 0;
    color: darkred;
`;

export default function Test({
    errors,
    meta,
}) {
    const {
        classname,
        name,
        time,
    } = meta;

    return (
        <div>
            <div>
                <Header style={!!(errors && errors.length) ? {color: 'darkred'} : {}}>{name}</Header>
                <Metadata>
                    {!isNaN(Number.parseFloat(time)) && (
                        <React.Fragment>
                            <span>{Number.parseFloat(time)}s</span>
                            {` / `}
                        </React.Fragment>
                    )}
                    <span>{classname}</span>
                </Metadata>
            </div>
            <div>
                {errors.map(({$: {message}, _: stacktrace}, index) => (
                    <div key={index}>
                        <ErrorMessage>{message}</ErrorMessage>
                        <pre>{stacktrace}</pre>
                    </div>
                ))}
            </div>
        </div>
    );
}
