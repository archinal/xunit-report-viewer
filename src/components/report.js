import React, { useState } from 'react';
import styled from 'styled-components';
import Test from './test';

const Heading = styled.h2`
  margin: 14px 0 14px;
`;

const SmallText = styled.div`
  color: slategrey;
`;

const StyledDl = styled.dl`
  display: flex;
  flex-direction: row;
`;

const StyledDd = styled.dd`
  display: inline;
  margin-inline-start: 0;
  margin-right: 7px;
`;

const StyledDt = styled.dt`
  display: inline;
`;

const RightMargin = styled.div`
  margin-right: 14px;
`;

const FilterForm = styled.form`
  display: flex;
  flex-direction: row;
`;

const FormLabel = styled.label`
  margin-right: 14px;
`;

const LabelText = styled.span`
  margin-right: 7px;
`;

const FilterText = styled.input`
  width: 210px;
`;

export default function Report({ tests, meta }) {
  const { name, tests: numTests, skipped, failures, timestamp } = meta;

  const [shouldShowFailed, setShouldShowFailed] = useState(true);
  const [shouldShowPassed, setShouldShowPassed] = useState(true);
  const [textFilter, setTextFilter] = useState('');

  return (
    <div>
      <div>
        <Heading>{name}</Heading>
        <SmallText>{timestamp}</SmallText>
        <StyledDl>
          <RightMargin
            style={{ color: numTests === failures ? 'green' : 'black' }}
          >
            <StyledDd>Tests</StyledDd>
            <StyledDt>{numTests}</StyledDt>
          </RightMargin>
          <RightMargin
            style={{ color: Number.parseInt(failures) ? 'red' : 'grey' }}
          >
            <StyledDd>Failures</StyledDd>
            <StyledDt>{failures}</StyledDt>
          </RightMargin>
          <RightMargin
            style={{ color: Number.parseInt(skipped) ? 'navy' : 'grey' }}
          >
            <StyledDd>Skipped</StyledDd>
            <StyledDt>{skipped}</StyledDt>
          </RightMargin>
        </StyledDl>
      </div>
      <FilterForm>
        <FormLabel>
          <LabelText>Failed</LabelText>
          <input
            type="checkbox"
            onChange={e => {
              setShouldShowFailed(e.target.checked);
            }}
            defaultChecked={true}
          />
        </FormLabel>
        <FormLabel>
          <LabelText>Passed</LabelText>
          <input
            type="checkbox"
            onChange={e => {
              setShouldShowPassed(e.target.checked);
            }}
            defaultChecked={true}
          />
        </FormLabel>
        <FormLabel>
          <LabelText>Filter by text</LabelText>
          <FilterText
            type="text"
            placeholder="Filter"
            onChange={e => {
              setTextFilter(e.target.value);
            }}
          />
        </FormLabel>
      </FilterForm>
      <div>
        {tests
          .filter(
            ({ $: { name }, error }) =>
              (error ? shouldShowFailed : shouldShowPassed) &&
              name.includes(textFilter)
          )
          .map(({ $: testMeta, error }, index) => (
            <Test
              meta={testMeta}
              errors={error || []}
              key={`${testMeta.name} _ ${index}`}
            />
          ))}
      </div>
    </div>
  );
}
