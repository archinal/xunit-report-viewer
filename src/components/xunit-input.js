import React from 'react';
import propTypes from 'prop-types';
import styled from 'styled-components';

const FlexForm = styled.form`
    display: flex;
    flex-direction: row;
    justify-content: stretch;
    align-items: flex-start;
`;

const FormLabel = styled.label`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    padding-right: 21px;
`;

const LabelText = styled.span`
    margin-bottom: 14px;
`;

const ResetButton = styled.button`
    align-self: center;
    width: 56px;
    height: 56px;
`;

export default function XunitInput ({
    onChangeValue,
}) {
    return (
        <FlexForm>
            <FormLabel>
                <LabelText>Paste report</LabelText>
                <textarea
                    rows={4}
                    onChange={(e) => onChangeValue(e.target.value)}
                />
            </FormLabel>
            <FormLabel>
                <LabelText>Upload from file</LabelText>
                <input
                    type="file"
                    placeholder="Choose file"
                    onChange={(event) => {
                        const reader = new FileReader();

                        reader.onload = function() {
                            onChangeValue(reader.result);
                        };

                        if (event.target.files && event.target.files[0]) {
                            try {
                                reader.readAsText(event.target.files[0]);
                            } catch (e) {
                                console.error('Error parsing uploaded file', e);
                            }
                        }
                    }}
                />
            </FormLabel>
            <ResetButton
                type="reset"
                onClick={() => onChangeValue('')}
            >
                Reset
            </ResetButton>
        </FlexForm>
    )
}

XunitInput.propTypes = {
    onChangeValue: propTypes.func,
    setIsExpanded: propTypes.func,
    isExpanded: propTypes.bool,
};
